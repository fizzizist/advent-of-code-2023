# Advent of Code 2023

It's that time of year again and I am excited to practice my Golang this year.

## Want to see the solutions?

Using Cobra, I have set up an easy to use command-line app like I have with my go-rosalind solutions. Here is an example usage:

```bash
go build
./advent-of-code-2023 day-1 --part2 <input_file>
```
