package main

import (
	"fmt"
	"os"

	"gitlab.com/fizzizist/advent-of-code-2023/cmd"
)

func main() {
	if err := cmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
