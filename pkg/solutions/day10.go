package solutions

import (
	"fmt"
	"log"

	"gitlab.com/fizzizist/advent-of-code-2023/pkg/io"
)

type coord struct {
	x int
	y int
}

// getNextCoord is used to follow a pipe from a prevCoo to a nextCoo
func getNextCoord(sym rune, coo coord, coordMap *map[coord]rune, prevCoo coord) (rune, coord, coord) {
	northCoo := coord{coo.x - 1, coo.y}
	westCoo := coord{coo.x, coo.y - 1}
	southCoo := coord{coo.x + 1, coo.y}
	eastCoo := coord{coo.x, coo.y + 1}
	north := (*coordMap)[northCoo]
	west := (*coordMap)[westCoo]
	south := (*coordMap)[southCoo]
	east := (*coordMap)[eastCoo]
	switch sym {
	case '|':
		if northCoo != prevCoo {
			return north, northCoo, coo
		}
		return south, southCoo, coo
	case '-':
		if westCoo != prevCoo {
			return west, westCoo, coo
		}
		return east, eastCoo, coo
	case 'F':
		if eastCoo != prevCoo {
			return east, eastCoo, coo
		}
		return south, southCoo, coo
	case 'J':
		if westCoo != prevCoo {
			return west, westCoo, coo
		}
		return north, northCoo, coo
	case '7':
		if westCoo != prevCoo {
			return west, westCoo, coo
		}
		return south, southCoo, coo
	case 'L':
		if northCoo != prevCoo {
			return north, northCoo, coo
		}
		return east, eastCoo, coo
	}
	fmt.Println("couldn't get next coord")
	return 0, coord{0, 0}, coo
}

// getNextLoc is meant to get the first next location starting from S
func getNextLoc(sym rune, coo coord, coordMap *map[coord]rune) (rune, coord) {
	northCoo := coord{coo.x - 1, coo.y}
	westCoo := coord{coo.x, coo.y - 1}
	southCoo := coord{coo.x + 1, coo.y}
	eastCoo := coord{coo.x, coo.y + 1}
	north := (*coordMap)[northCoo]
	west := (*coordMap)[westCoo]
	south := (*coordMap)[southCoo]
	east := (*coordMap)[eastCoo]

	if north == '|' || north == '7' || north == 'F' || north == 'S' {
		return north, northCoo
	}

	if west == '-' || west == 'F' || west == 'L' || west == 'S' {
		return west, westCoo
	}

	if south == '|' || south == 'J' || south == 'L' || south == 'S' {
		return south, southCoo
	}

	if east == '-' || east == '7' || east == 'J' || east == 'S' {
		return east, eastCoo
	}
	fmt.Println("Something went horribly wrong")
	return 0, coord{0, 0}
}

func DayTen(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	pipeMap := make([][]rune, len(lines))
	for i, line := range lines {
		pipeMap[i] = []rune(line)
	}

	coordMap := make(map[coord]rune)
	var sLoc coord
	for i, row := range pipeMap {
		for j, sym := range row {
			coordMap[coord{i, j}] = sym
			if sym == 'S' {
				sLoc = coord{i, j}
			}
		}
	}
	fmt.Println(sLoc)
	currPipe, currCoord := getNextLoc('S', sLoc, &coordMap)
	prevCoo := sLoc
	pipeLen := 1
	for currPipe != 'S' {
		currPipe, currCoord, prevCoo = getNextCoord(currPipe, currCoord, &coordMap, prevCoo)
		pipeLen++
	}
	fmt.Println(pipeLen / 2)
}

func DayTenPartTwo(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	// generate the map
	pipeMap := make([][]rune, len(lines))
	for i, line := range lines {
		pipeMap[i] = []rune(line)
	}

	// generate coordinate : symbol map
	coordMap := make(map[coord]rune)
	var sLoc coord
	for i, row := range pipeMap {
		for j, sym := range row {
			coordMap[coord{i, j}] = sym
			if sym == 'S' {
				sLoc = coord{i, j}
			}
		}
	}

	currPipe, currCoord := getNextLoc('S', sLoc, &coordMap)
	prevCoo := sLoc
	borderMap := make([][]bool, len(pipeMap))
	for i := range borderMap {
		borderMap[i] = make([]bool, len(pipeMap[i]))
	}
	borderMap[sLoc.x][sLoc.y] = true
	borderMap[currCoord.x][currCoord.y] = true
	// constructs a full list of where the pipe loop is
	for currPipe != 'S' {
		currPipe, currCoord, prevCoo = getNextCoord(currPipe, currCoord, &coordMap, prevCoo)
		borderMap[currCoord.x][currCoord.y] = true
	}

	total := 0
	for i, row := range borderMap {
		within := false
		var inWall rune
		for j, isBorder := range row {
			if isBorder {
				if pipeMap[i][j] == '|' {
					within = !within
				}
				if (pipeMap[i][j] == '7' && inWall == 'L') || (pipeMap[i][j] == 'J' && inWall == 'F') {
					// crossing over wall only if the corners are opposing
					within = !within
				}
				if pipeMap[i][j] == 'F' || pipeMap[i][j] == 'L' || pipeMap[i][j] == 'S' {
					inWall = pipeMap[i][j]
				}
			}
			if within && !isBorder {
				total++
				// show inner tiles in red
				fmt.Print("\033[0;31mI\033[0m")
			} else {
				// highlight the pipes in green
				if isBorder {
					fmt.Printf("\033[0;32m%c\033[0m", pipeMap[i][j])
				} else {
					// print everything else in white
					fmt.Print(string(pipeMap[i][j]))
				}
			}
		}
		fmt.Println()
	}
	fmt.Println()
	fmt.Println(total)
}
