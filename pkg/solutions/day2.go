package solutions

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/fizzizist/advent-of-code-2023/pkg/io"
)

type game struct {
	id    int
	red   int
	green int
	blue  int
}

// getGame transforms an input line into a game object,
// where the values for the cubes are the minimim number of cubes required
// for that game.
func getGame(line string) game {
	// split ID from cube data
	gameData := strings.Split(line, ":")
	// get ID and make into int
	gameIdStr := strings.Split(gameData[0], " ")[1]
	gameId, err := strconv.Atoi(gameIdStr)
	if err != nil {
		fmt.Printf("%s is not a number\n", gameIdStr)
	}
	// initial game state
	thisGame := game{gameId, 0, 0, 0}

	// separate game rounds and iterate through them
	// reseting cube values whenever they are larger than is currently set
	games := strings.Split(gameData[1], ";")
	for _, singleGame := range games {
		colorNums := strings.Split(singleGame, ",")
		for _, colorNum := range colorNums {
			colorNumArr := strings.Split(colorNum, " ")
			num, err := strconv.Atoi(colorNumArr[1])
			if err != nil {
				fmt.Printf("%s is not a number (inner)\n", colorNumArr[1])
			}
			switch colorNumArr[2] {
			case "red":
				if num > thisGame.red {
					thisGame.red = num
				}
			case "green":
				if num > thisGame.green {
					thisGame.green = num
				}
			case "blue":
				if num > thisGame.blue {
					thisGame.blue = num
				}
			}
		}
	}
	return thisGame
}

// isPossible checks if a game was possible for the given cube values
func isPossible(g game) bool {
	if g.red > 12 {
		return false
	}
	if g.green > 13 {
		return false
	}
	if g.blue > 14 {
		return false
	}
	return true
}

func DayTwo(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	gameSum := 0
	for _, line := range lines {
		lineGame := getGame(line)
		// add up possible game IDs
		if isPossible(lineGame) {
			gameSum += lineGame.id
		}
	}
	fmt.Println(gameSum)
}

func DayTwoPartTwo(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	gameSum := 0
	for _, line := range lines {
		lineGame := getGame(line)
		// add game powers
		gameSum += (lineGame.red * lineGame.green * lineGame.blue)
	}
	fmt.Println(gameSum)
}
