package solutions

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"unicode"

	"gitlab.com/fizzizist/advent-of-code-2023/pkg/io"
)

type wordNum struct {
	word string
	num  rune
}

// hasNum checks if a number exists in a string return a bool for if it exists
// and a rune representation of the int for that number.
func hasNum(s string) (bool, rune) {
	wordNums := []wordNum{
		{"zero", '0'},
		{"one", '1'},
		{"two", '2'},
		{"three", '3'},
		{"four", '4'},
		{"five", '5'},
		{"six", '6'},
		{"seven", '7'},
		{"eight", '8'},
		{"nine", '9'},
	}
	for _, num := range wordNums {
		if strings.Contains(s, num.word) {
			return true, num.num
		}
	}
	return false, '0'
}

func DayOnePartTwo(filename string) {
	// read in the file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	calSum := 0
	for _, line := range lines {
		// digits will hold all numbers from the line
		var digits []rune
		num := ""

		for _, c := range line {
			// numerical representation
			if unicode.IsDigit(c) {
				digits = append(digits, c)
				num = ""
				// otherwise, we search for numbers in the text
			} else {
				num += string(c)
				isNum, runeNum := hasNum(num)
				if isNum {
					digits = append(digits, runeNum)
					num = string(c)
				}
			}
		}
		// get the first and last digit and string them together
		calValStr := string(digits[0]) + string(digits[len(digits)-1])
		// convert them to an int
		calVal, err := strconv.Atoi(calValStr)
		if err != nil {
			fmt.Println(err)
		}
		// add to the final total
		calSum += calVal
	}

	fmt.Println(calSum)
}
