package solutions

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"unicode"
	"sort"

	"gitlab.com/fizzizist/advent-of-code-2023/pkg/io"
)

type HandType int

const (
	FiveOfAKind HandType = iota
	FourOfAKind
	FullHouse
	ThreeOfAKind
	TwoPair
	OnePair
	HighCard
)

type hand struct {
	cards []rune
	type_ HandType
	bid int
}

func cardValue(card rune, joker bool) int {
	if unicode.IsDigit(card) {
		intNum, err := strconv.Atoi(string(card))
		if err != nil {
			log.Fatalf("Failed to parse card: %s", err)
		}
		return intNum
	}
	switch card {
	case 'T':
		return 10
	case 'J':
		if joker {
			return 1
		} else {
			return 11
		}
	case 'Q':
		return 12
	case 'K':
		return 13
	case 'A':
		return 14
	}
	return 0
}


func compareCards(a rune, b rune, joker bool) bool {
	return cardValue(a, joker) > cardValue(b, joker)
}

func compareCardsDesc(a rune, b rune, joker bool) bool {
	return cardValue(a, joker) < cardValue(b, joker)
}


func determineHandType(handCards []rune) HandType {
	cards := make([]rune, len(handCards))
	copy(cards, handCards)
	sort.Slice(cards, func(i, j int) bool {
		return compareCards(cards[i], cards[j], false)
	})

	groups := [][]rune{{cards[0]}}

	currentCard := cards[0]

	for i := 1; i < len(cards); i++ {
		if cards[i] == currentCard {
			groups[len(groups)-1] = append(groups[len(groups)-1], cards[i])
		} else {
			groups = append(groups, []rune{cards[i]})
			currentCard = cards[i]
		}
	}

	if len(groups) == 1 {
		return FiveOfAKind
	}

	if len(groups) == 2 {
		if len(groups[0]) == 4 || len(groups[1]) == 4 {
			return FourOfAKind
		}
		return FullHouse
	}

	if len(groups) == 3 {
		if len(groups[0]) == 3 || len(groups[1]) == 3 || len(groups[2]) == 3 {
			return ThreeOfAKind
		}
		return TwoPair
	}

	if len(groups) == 4 {
		return OnePair
	}

	return HighCard
}

func determineHandTypeJoker(handCards []rune) HandType {
	cards := make([]rune, len(handCards))
	copy(cards, handCards)
	sort.Slice(cards, func(i, j int) bool {
		return compareCards(cards[i], cards[j], true)
	})

	var currentCard rune

	groups := [][]rune{}

	jokers := 0

	for i := 0; i < len(cards); i++ {
		if cards[i] == 'J' {
			jokers++
			currentCard = cards[i]
			continue
		}
		if cards[i] == currentCard {
			groups[len(groups)-1] = append(groups[len(groups)-1], cards[i])
		} else {
			groups = append(groups, []rune{cards[i]})
			currentCard = cards[i]
		}
	}

	// all same or all jokers
	if len(groups) == 1 || len(groups) == 0 {
		return FiveOfAKind
	}

	if len(groups) == 2 {
		if len(groups[0]) >= 2 && len(groups[1]) >= 2 {
			return FullHouse
		}
		return FourOfAKind
	}

	if len(groups) == 3 {
		twoCount := 0
		for _, group := range groups {
			if len(group) == 2 {
				twoCount++
			}
			if twoCount == 2 {
				return TwoPair
			}
		}
		return ThreeOfAKind
	}

	if len(groups) == 4 {
		return OnePair
	}

	return HighCard
}

func compareHands(a hand, b hand, joker bool) bool {
	if a.type_ != b.type_ {
		return a.type_ > b.type_
	}

	for i := 0; i < len(a.cards); i++ {
		if a.cards[i] != b.cards[i] {
			return compareCardsDesc(a.cards[i], b.cards[i], joker)
		}
	}

	return false
}

func parseHand(line string, joker bool) hand {
	rawData := strings.Split(line, " ")
	cards := []rune(rawData[0])
	bid, err := strconv.Atoi(rawData[1])
	if err != nil {
		log.Fatalf("Failed to parse bid: %s", err)
	}
	if joker {
		return hand{cards, determineHandTypeJoker(cards), bid}
	}
	return hand{cards, determineHandType(cards), bid}
}


func DaySeven(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	hands := make([]hand, len(lines))

	for i, line := range lines {
		hands[i] = parseHand(line, false)
	}
	sort.Slice(hands, func(i, j int) bool {
		return compareHands(hands[i], hands[j], false)
	})
	total := 0
	for i, hand := range hands {
		total += hand.bid * (i + 1)
	}
	fmt.Println(total)
}

func DaySevenPartTwo(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	hands := make([]hand, len(lines))

	for i, line := range lines {
		hands[i] = parseHand(line, true)
	}
	sort.Slice(hands, func(i, j int) bool {
		return compareHands(hands[i], hands[j], true)
	})
	total := 0
	for i, hand := range hands {
		total += hand.bid * (i + 1)
	}
	fmt.Println(total)
}
