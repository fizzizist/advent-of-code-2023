package solutions

import (
	"fmt"
	"log"
	"strings"

	"gitlab.com/fizzizist/advent-of-code-2023/pkg/io"
)

type directions struct {
	right string
	left string
}

func DayEight(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	var instructions []rune
	dirMap := make(map[string]directions)
	for i, line := range lines {
		if i == 0 {
			instructions = []rune(line)
			continue
		}
		if i == 1 {
			continue
		} else {
			rawData := strings.Split(line, " = ")
			rawDirs := strings.Split(rawData[1], ", ")
			right := strings.Split(rawDirs[1], ")")[0]
			left := strings.Split(rawDirs[0], "(")[1]
			dirMap[rawData[0]] = directions{right, left}
		}
	}
	currLoc := "AAA"
	stepCount := 0
	instIdx := 0
	for currLoc != "ZZZ" {
		if instIdx == len(instructions) {
			instIdx = 0
		}
		if instructions[instIdx] == 'R' {
			currLoc = dirMap[currLoc].right
		} else {
			currLoc = dirMap[currLoc].left
		}
		stepCount++
		instIdx++
	}
	fmt.Println(stepCount)
}

// checkAllLocs returns true until all locs have a Z at the end.
func checkAllLocs(spans []int) bool {
	for _, span := range spans {
		if span == 0 {
			return true
		}
	}
	return false
}

func checkOneLoc(loc string) bool {
	endLetter := []rune(loc)[2]
	if endLetter == 'Z' {
		return true
	}
	return false
}

func checkEqualSpans(s []int) bool {
	currSpan := s[0]
	for _, span := range s {
		if span != currSpan {
			return true
		}
	}
	return false
}
	
// Function to calculate GCD (greatest common divisor)
func gcd(a, b int) int {
	if b == 0 {
		return a
	}
	return gcd(b, a % b)
}

// Function to calculate LCM (least common multiple)
func lcm(a, b int) int {
	return (a / gcd(a, b)) * b
}

// Function to calculate LCM of an array
func lcmOfArray(numbers []int) int {
	if len(numbers) == 2 {
		return lcm(numbers[0], numbers[1])
	} else {
		return lcm(numbers[0], lcmOfArray(numbers[1:]))
	}
}

func DayEightPartTwo(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	var instructions []rune
	dirMap := make(map[string]directions)
	for i, line := range lines {
		if i == 0 {
			instructions = []rune(line)
			continue
		}
		if i == 1 {
			continue
		} else {
			rawData := strings.Split(line, " = ")
			rawDirs := strings.Split(rawData[1], ", ")
			right := strings.Split(rawDirs[1], ")")[0]
			left := strings.Split(rawDirs[0], "(")[1]
			dirMap[rawData[0]] = directions{right, left}
		}
	}
	currLocs := make([]string, 0)
	for key := range dirMap {
		endLetter := []rune(key)[2]
		if endLetter == 'A' {
			currLocs = append(currLocs, key)
		}
	}
		
	stepCount := 0
	instIdx := 0
	zSpans := make([]int, len(currLocs))
	for checkAllLocs(zSpans) {
		if stepCount > 1000000 {
			break
		}
		if instIdx == len(instructions) {
			instIdx = 0
		}
		if instructions[instIdx] == 'R' {
			for i, loc := range currLocs {
					currLocs[i] = dirMap[loc].right
			} 
		} else {
			for i, loc := range currLocs {
					currLocs[i] = dirMap[loc].left
			} 
		}
		for i, loc := range currLocs {
			if checkOneLoc(loc) {
				zSpans[i] = stepCount
			}
		}
		stepCount++
		instIdx++
	}

	for i, span := range zSpans {
		zSpans[i] = span + 1
	}

	fmt.Println(lcmOfArray(zSpans))
}
