package solutions

import (
	"fmt"
	"log"
	"strconv"
	"unicode"

	"gitlab.com/fizzizist/advent-of-code-2023/pkg/io"
)

func DayOne(filename string) {
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	calSum := 0
	for _, line := range lines {
		// pull just the digits out of the line
		var digits []rune
		for _, c := range line {
			if unicode.IsDigit(c) {
				digits = append(digits, c)
			}
		}
		calValStr := string(digits[0]) + string(digits[len(digits)-1])
		calVal, err := strconv.Atoi(calValStr)
		if err != nil {
			fmt.Println(err)
		}
		calSum += calVal
	}

	fmt.Println(calSum)
}
