package solutions

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/kinsey40/pbar"
	"gitlab.com/fizzizist/advent-of-code-2023/pkg/io"
)

type rangeMap struct {
	from int
	to int
	rangeLen int
}

type edge struct {
	from string
	to string
	rangeMaps []rangeMap
}

type seedRange struct {
	init int
	ran int
}

// parseSeeds parses a line of text into seed ints
func parseSeeds(line string) []int {
	data := strings.Split(line, ":")[1]
	nums := strings.Split(data, " ")
	retNums := make([]int, 0)
	for _, num := range nums {
		intNum, err := strconv.Atoi(num)
		if err == nil {
			retNums = append(retNums, intNum)
		}
	}
	return retNums
}

// parseSeedRanges parses a line of text into seedRange structs
func parseSeedRanges(line string) []seedRange {
	retNums := parseSeeds(line)
	retRanges := make([]seedRange, 0)
	for i := 0; i < len(retNums); i += 2 {
		retRanges = append(retRanges, seedRange{retNums[i], retNums[i+1]})
	}
	return retRanges
}

// parseEdge parses a slice of strings into an edge struct
func parseEdge(lines []string) edge {
	var e edge
	for i, line := range lines {
		if i == 0 {
			edgeToFromRaw := strings.Split(line, " ")[0]
			edgeToFrom := strings.Split(edgeToFromRaw, "-to-")
			e = edge{edgeToFrom[0], edgeToFrom[1], make([]rangeMap, 0)}
		} else {
			rawNums := strings.Split(line, " ")
			nums := make([]int, 0)
			for _, num := range rawNums {
				intNum, err := strconv.Atoi(num)
				if err != nil {
					fmt.Printf("%s is not a number", num)
				}
				nums = append(nums, intNum)
			}
			e.rangeMaps = append(e.rangeMaps, rangeMap{nums[1], nums[0], nums[2]})
		}
	}
	return e
}

// traverseEdge traverses an edge with a given number
func traverseEdge(e edge, num int) int {
	for _, rMap := range e.rangeMaps {
		if num <= (rMap.from + rMap.rangeLen) && num >= rMap.from {
			return ((num - rMap.from) + rMap.to)
		}
	}
	// wasn't in any range
	return num
}

// getEdges parses a slice of strings into a slice of edges
func getEdges(lines []string) []edge {
	rawEdges := make([][]string, 0)
	rawEdge := make([]string, 0)
	for i, line := range lines {
		if i == 0 || i == 1 {
			continue
		} else if line == "" {
			rawEdges = append(rawEdges, rawEdge)
			rawEdge = nil
		} else {
			rawEdge = append(rawEdge, line)
		}
	}
	if rawEdge != nil {
		rawEdges = append(rawEdges, rawEdge)
	}

	edges := make([]edge, 0)
	for _, rEdge := range rawEdges {
		edges = append(edges, parseEdge(rEdge))
	}
	return edges
}


func DayFive(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}
	seeds := parseSeeds(lines[0])
	edges := getEdges(lines)

	seedLocations := make([]int, 0)
	node := "seed"
	for _, seed := range seeds {
		transNum := seed
		for node != "location" {
			for _, e := range edges {
				if node == e.from {
					transNum = traverseEdge(e, transNum)
					node = e.to
				}
			}
		}
		seedLocations = append(seedLocations, transNum)
		node = "seed"
	}

	lowest := seedLocations[0]
	for _, loc := range seedLocations {
		if loc < lowest {
			lowest = loc
		}
	}
	fmt.Println(lowest)
}

func DayFivePartTwo(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}
	seedRanges := parseSeedRanges(lines[0])
	edges := getEdges(lines)

	// algo is pretty slow, so pbar
	p, _ := pbar.Pbar(len(seedRanges))
	p.Initialize()

	seedLocations := make([]int, 0)
	node := "seed"
	for _, seedR := range seedRanges {
		for seed := seedR.init; seed < (seedR.ran + seedR.init); seed++ {
			transNum := seed
			for node != "location" {
				for _, e := range edges {
					if node == e.from {
						transNum = traverseEdge(e, transNum)
						node = e.to
					}
				}
			}
			seedLocations = append(seedLocations, transNum)
			node = "seed"
		}
		p.Update()
	}

	lowest := seedLocations[0]
	for _, loc := range seedLocations {
		if loc < lowest {
			lowest = loc
		}
	}
	fmt.Println("User beware! Sometimes this algorith outputs an answer that is off by 1. I don't have time to debug that. I already got my star")
	fmt.Println(lowest)
}
