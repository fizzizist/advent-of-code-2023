package solutions

import (
	"fmt"
	"log"

	"gitlab.com/fizzizist/advent-of-code-2023/pkg/io"
)



func DayFourteen(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	dish := make([][]rune, len(lines[0]))
	for i := 0; i < len(lines); i++ {
		dish[i] = make([]rune, len(lines))
	}

	for i, line := range lines {
		for j, sym := range []rune(line) {
			dish[j][i] = sym
		}
	}

	total := 0
	for _, col := range dish {
		roundRockNum := 0
		sectionLen := 0
		colLen := len(col)
		for j, sym := range col {
			switch sym {
			case '#':
				for q := 0; q < roundRockNum; q++ {
					total += (colLen - j) + (sectionLen - q)
				}
				roundRockNum = 0
				sectionLen = 0
			case '.':
				sectionLen++
			case 'O':
				roundRockNum++
				sectionLen++
			}
		}
		if roundRockNum > 0 {
			for q := 0; q < roundRockNum; q++ {
				total += (sectionLen - q)
			}
		}
	}
	fmt.Println(total)
}

func DayFourteenPartTwo(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}
	fmt.Println(lines)
}
