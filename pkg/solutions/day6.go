package solutions

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"unicode"

	"gitlab.com/fizzizist/advent-of-code-2023/pkg/io"
	"github.com/spatialcurrent/go-math/pkg/math"
)

// parseIntList parses a line of text into a slice of ints
func parseIntList(line string) []int {
	rawData := strings.Split(line, ":")[1]
	rawNums := strings.Split(rawData, " ")
	retNums := make([]int, 0)
	for _, num := range rawNums {
		intNum, err := strconv.Atoi(num)
		if err == nil {
			retNums = append(retNums, intNum)
		}
	}
	return retNums
}

// parseInt parses a line of text into an int
func parseInt(line string) int {
	rawData := strings.Split(line, ":")[1]
	strNum := ""
	for _, num := range rawData {
		if unicode.IsDigit(num) {
			strNum += string(num)
		}
	}
	intNum, err := strconv.Atoi(strNum)
	if err != nil {
		fmt.Printf("Failed to parse int: %s", err)
	}
	return intNum
}

// getNumWays returns the number of ways to win a race
func getNumWays(time int, distance int) int {
	lower := 1
	upper := time - 1
	wins := 0
	for upper > 1 {
		if (lower * upper) > distance {
			wins++
		}
		lower++
		upper--
	}
	return wins
}

func DaySix(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	times := parseIntList(lines[0])
	distances := parseIntList(lines[1])

	ways := make([]int, 0)
	for i := range times {
		ways = append(ways, getNumWays(times[i], distances[i]))
	}
	fmt.Println(math.Product(ways))
}

func DaySixPartTwo(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}
	time := parseInt(lines[0])
	distance := parseInt(lines[1])

	ways := getNumWays(time, distance)
	fmt.Println(ways)
}
