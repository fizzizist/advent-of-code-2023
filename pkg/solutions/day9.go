package solutions

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/fizzizist/advent-of-code-2023/pkg/io"
)

// getNextSeq gets the next sequence and determines if that sequence has all the same value or not.
func getNextSeq(seq []int) ([]int, bool) {
	allSame := true
	nextSeq := make([]int, (len(seq) - 1))
	for i := 0; i < (len(seq) - 1); i++ {
		nextSeq[i] = seq[i+1] - seq[i]
		if i > 0 {
			if nextSeq[i] != nextSeq[i-1] {
				allSame = false
			}
		}
	}
	return nextSeq, allSame
}

// genNextNum recursively gets the next num in the given sequence
func getNextNum(seq []int) int {
	nextSeq, allSame := getNextSeq(seq)
	if allSame {
		return seq[len(seq)-1] + nextSeq[0]
	}
	return seq[len(seq)-1] + getNextNum(nextSeq)
}

// getPrevNum recursively gets the previous num in the given sequence
func getPrevNum(seq []int) int {
	nextSeq, allSame := getNextSeq(seq)
	if allSame {
		return seq[0] - nextSeq[0]
	}
	return seq[0] - getPrevNum(nextSeq)
}

// parseLine parses a string line into an int sequence
func parseLine(line string) []int {
	rawNums := strings.Split(line, " ")
	seqNums := make([]int, len(rawNums))
	for i, rawNum := range rawNums {
		intNum, err := strconv.Atoi(rawNum)
		if err != nil {
			fmt.Println("%s is not a number", rawNum)
		}
		seqNums[i] = intNum
	}
	return seqNums
}

// getSeqs produces an [][]int of sequences given all lines from the input
func getSeqs(lines []string) [][]int {
	seqs := make([][]int, len(lines))
	for i, line := range lines {
		seqs[i] = parseLine(line)
	}
	return seqs
}

func DayNine(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	seqs := getSeqs(lines)

	total := 0
	for _, seq := range seqs {
		total += getNextNum(seq)
	}
	fmt.Println(total)

}

func DayNinePartTwo(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	seqs := getSeqs(lines)

	total := 0
	for _, seq := range seqs {
		total += getPrevNum(seq)
	}
	fmt.Println(total)
}
