package solutions

import (
	"fmt"
	"log"

	"gitlab.com/fizzizist/advent-of-code-2023/pkg/io"
)

type galaxy struct {
	id       int
	position coord
}

func getExpandedUniverse(lines []string) [][]rune {
	// make universe and expand vertically
	preUniverse := make([][]rune, 0)
	horGal := make([]bool, len(lines[0]))
	for _, line := range lines {
		noGalaxy := true
		row := []rune(line)
		preUniverse = append(preUniverse, row)
		for j, sym := range line {
			if sym == '#' {
				noGalaxy = false
				horGal[j] = true
			}
		}
		if noGalaxy {
			preUniverse = append(preUniverse, row)
		}
	}

	universe := make([][]rune, len(preUniverse))
	// expand horizontally
	for i, row := range preUniverse {
		newRow := make([]rune, 0)
		for j, sym := range row {
			if horGal[j] {
				newRow = append(newRow, sym)
			} else {
				newRow = append(newRow, '.')
				newRow = append(newRow, '.')
			}
		}
		universe[i] = newRow
	}

	return universe
}

// getGalaxies returns an array of galaxy structs
func getGalaxies(universe [][]rune) []galaxy {
	galaxies := make([]galaxy, 0)
	for i, row := range universe {
		for j, sym := range row {
			if sym == '#' {
				galaxies = append(galaxies, galaxy{id: len(galaxies), position: coord{x: i, y: j}})
			}
		}
	}
	return galaxies
}

func getShortestPath(galaxy1 galaxy, galaxy2 galaxy) int {
	steps := 0
	movingPos := galaxy1.position
	for movingPos != galaxy2.position {
		if movingPos.x < galaxy2.position.x {
			movingPos.x++
		} else if movingPos.x > galaxy2.position.x {
			movingPos.x--
		} else if movingPos.y < galaxy2.position.y {
			movingPos.y++
		} else if movingPos.y > galaxy2.position.y {
			movingPos.y--
		}
		steps++
	}
	return steps
}

// expandGalaxies changes galaxy positions as if the universe of expanded by times
func expandGalaxies(galaxies []galaxy, universe [][]rune, times int) []galaxy {
	vPos := 0
	horGal := make([]bool, len(universe[0]))
	for _, row := range universe {
		hasGalaxy := false
		for j, sym := range row {
			if sym == '#' {
				hasGalaxy = true
				horGal[j] = true
			}
		}
		if !hasGalaxy {
			for j := range galaxies {
				if galaxies[j].position.x > vPos {
					galaxies[j].position.x += times
				}
			}
			vPos += times
		}
		vPos++
	}
	vPos = 0
	for _, hasGal := range horGal {
		if !hasGal {
			for j := range galaxies {
				if galaxies[j].position.y > vPos {
					galaxies[j].position.y += times
				}
			}
			vPos += times
		}
		vPos++
	}
	return galaxies
}

func DayEleven(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	universe := getExpandedUniverse(lines)
	galaxies := getGalaxies(universe)

	total := 0
	for i, galaxy1 := range galaxies {
		for j := i + 1; j < len(galaxies); j++ {
			total += getShortestPath(galaxy1, galaxies[j])
		}
	}
	fmt.Println(total)
}

func DayElevenPartTwo(filename string, times int) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	universe := make([][]rune, len(lines))
	for i, line := range lines {
		universe[i] = []rune(line)
	}

	galaxies := getGalaxies(universe)
	fmt.Println(galaxies)
	galaxies = expandGalaxies(galaxies, universe, times)
	fmt.Println(galaxies)

	total := 0
	for i, galaxy1 := range galaxies {
		for j := i + 1; j < len(galaxies); j++ {
			total += getShortestPath(galaxy1, galaxies[j])
		}
	}
	fmt.Println(total)
}
