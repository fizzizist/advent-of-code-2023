package solutions

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"slices"

	"gitlab.com/fizzizist/advent-of-code-2023/pkg/io"
)

// parseSpacedNums takes a string of numbers separated by spaces and returns a slice of ints
func parseSpacedNums(s string) []int {
	nums := []int{}
	for _, num := range strings.Split(s, " ") {
		n, err := strconv.Atoi(num)
		if err == nil {
			nums = append(nums, n)
		}
	}
	return nums
}

func DayFour(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}
	cardTotal := 0
	for _, line := range lines {
		rawData := strings.Split(line, ":")[1]
		rawWinHave := strings.Split(rawData, "|")
		winNums := parseSpacedNums(rawWinHave[0])
		haveNums := parseSpacedNums(rawWinHave[1])
		total := 0
		for _, num := range winNums {
			if slices.Contains(haveNums, num) {
				if total == 0 {
					total = 1
				} else {
					total *= 2
				}
			}
		}
		cardTotal += total
	}
	fmt.Println(cardTotal)
}

func DayFourPartTwo(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	cardTotal := 0
	cardCopies := make(map[int]int)	
	for cardNum, line := range lines {
		rawData := strings.Split(line, ":")[1]
		rawWinHave := strings.Split(rawData, "|")
		winNums := parseSpacedNums(rawWinHave[0])
		haveNums := parseSpacedNums(rawWinHave[1])
		wins := 0
		for _, num := range winNums {
			if slices.Contains(haveNums, num) {
				wins++
			}
		}
		for cardCopies[cardNum] >= 0 {
			for j := 1; j <= wins; j++ {
				cardCopies[cardNum + j]++
			}
			cardCopies[cardNum]--
			cardTotal++
		}
	}
	fmt.Println(cardTotal)
}
