package solutions

import (
	"fmt"
	"log"
	"strconv"
	"unicode"

	"gitlab.com/fizzizist/advent-of-code-2023/pkg/io"
)

// strToInt converts a string to num without throwing error, but just printing
// I want to see a message in a failure case, but i don't want to have to handle it every time.
func strToInt(num string) int {
	intNum, err := strconv.Atoi(num)
	if err != nil {
		fmt.Printf("%s is not a number", num)
	}
	return intNum
}

// coords represents coordinates in the schematic
type coords struct {
	x int
	y int
}

// getAdjacentCoords returns the adjacent coords for a given num and starting cursor position.
func getAdjacentCoords(schematic *[][]rune, num string, x int, y int) []coords {
	toCheck := []coords{}
	lowerY := y - (len(num) + 1)
	schemaWidth := len((*schematic)[0])

	// current line
	if y < schemaWidth {
		toCheck = append(toCheck, coords{x, y})
	}
	if lowerY >= 0 {
		toCheck = append(toCheck, coords{x, lowerY})
	}

	// next line
	if (x + 1) < len(*schematic) {
		for i := lowerY; i <= y; i++ {
			if i >= 0 && i < schemaWidth {
				toCheck = append(toCheck, coords{x + 1, i})
			}
		}
	}

	// prev line
	if (x - 1) >= 0 {
		for i := lowerY; i <= y; i++ {
			if i >= 0 && i < schemaWidth {
				toCheck = append(toCheck, coords{x - 1, i})
			}
		}
	}

	return toCheck
}

// hasSymbol checks the schematic around the given num with the cursor an x and y
// It checks if there are any symbols. If it finds one it immediately returns
func hasSymbol(schematic *[][]rune, num string, x int, y int) bool {
	toCheck := getAdjacentCoords(schematic, num, x, y)
	// check and return
	for _, coord := range toCheck {
		if !unicode.IsDigit((*schematic)[coord.x][coord.y]) && (*schematic)[coord.x][coord.y] != '.' {
			return true
		}
	}
	return false
}

// findGears fills the gearMap with coord: [num] values by taking a num, looking for a gear symbol
// adjacent to it, and updating gearMap if it finds one.
func findGears(schematic *[][]rune, num string, x int, y int, gearMap map[coords][]int) map[coords][]int {
	toCheck := getAdjacentCoords(schematic, num, x, y)

	for _, coord := range toCheck {
		if (*schematic)[coord.x][coord.y] == '*' {
			gearMap[coord] = append(gearMap[coord], strToInt(num))
		}
	}
	return gearMap
}

// getSchematic turns lines []string into a matrix of runes
func getSchematic(lines *[]string) [][]rune {
	schematic := make([][]rune, len(*lines))
	for i, line := range *lines {
		schematic[i] = []rune(line)
	}
	return schematic
}

func DayThree(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	// turn lines into a rune matrix
	schematic := getSchematic(&lines)

	partNums := make([]int, 0)
	for i, line := range schematic {
		num := ""
		for j, r := range line {
			if unicode.IsDigit(r) {
				num += string(r)
			} else if r == '.' {
				if num != "" {
					if hasSymbol(&schematic, num, i, j) {
						partNums = append(partNums, strToInt(num))
					}
					num = ""
				}
			} else if !unicode.IsDigit(r) && num != "" {
				// this case means we dont have to check the schematic.
				// We know the num has an adjacent symbol
				partNums = append(partNums, strToInt(num))
				num = ""
			}
		}
		// if number is right at the end of the line
		if num != "" {
			if hasSymbol(&schematic, num, i, len(line)) {
				partNums = append(partNums, strToInt(num))
			}
		}
	}

	partSum := 0
	for _, partNum := range partNums {
		partSum += partNum
	}
	fmt.Println(partSum)

}

func DayThreePartTwo(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	// turn lines into a rune matrix
	schematic := getSchematic(&lines)

	gearMap := make(map[coords][]int)
	for i, line := range schematic {
		num := ""
		for j, r := range line {
			if unicode.IsDigit(r) {
				num += string(r)
			} else if !unicode.IsDigit(r) && num != "" {
				gearMap = findGears(&schematic, num, i, j, gearMap)
				num = ""
			}
		}
		// if number is right at the end of the line
		if num != "" {
			gearMap = findGears(&schematic, num, i, len(line), gearMap)
		}
	}

	gearSum := 0
	for _, nums := range gearMap {
		if len(nums) == 2 {
			gearSum += nums[0] * nums[1]
		}
	}
	fmt.Println(gearSum)

}
