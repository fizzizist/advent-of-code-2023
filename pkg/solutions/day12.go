package solutions

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/fizzizist/advent-of-code-2023/pkg/io"
)



func DayTwelve(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	for _, line := range lines {
		data := strings.Split(line, " ")
		springs := []rune(data[0])
		fmt.Println(springs)
		rawNums := strings.Split(data[1], ",")
		nums := make([]int, len(rawNums))
		for _, rNum := range rawNums {
			num, err := strconv.Atoi(rNum)
			if err != nil {
				log.Fatalf("Failed to convert string to int: %s", err)
			}
			nums = append(nums, num)
		}
	}
		

	
}

func DayTwelvePartTwo(filename string) {
	// read in file
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}
	fmt.Println(lines)
}
