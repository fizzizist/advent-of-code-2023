package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/fizzizist/advent-of-code-2023/pkg/solutions"
)

var dayFourteen = &cobra.Command{
	Use:   "day-14",
	Short: "Runs the day 14 puzzle.",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if partTwo {
			solutions.DayFourteenPartTwo(args[0])
		} else {
			solutions.DayFourteen(args[0])
		}
	},
}

func init() {
	dayFourteen.Flags().BoolVar(&partTwo, "part2", false, "Run part 2 solution")
	rootCmd.AddCommand(dayFourteen)
}
