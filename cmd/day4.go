package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/fizzizist/advent-of-code-2023/pkg/solutions"
)

var dayFour = &cobra.Command{
	Use:   "day-4",
	Short: "Runs the day 1 puzzle.",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if partTwo {
			solutions.DayFourPartTwo(args[0])
		} else {
			solutions.DayFour(args[0])
		}
	},
}

func init() {
	dayFour.Flags().BoolVar(&partTwo, "part2", false, "Run part 2 solution")
	rootCmd.AddCommand(dayFour)
}
