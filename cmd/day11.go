package cmd

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/fizzizist/advent-of-code-2023/pkg/solutions"
)

var dayEleven = &cobra.Command{
	Use:   "day-11",
	Short: "Runs the day 11 puzzle.",
	Args:  cobra.MinimumNArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		if partTwo {
			num, err := strconv.Atoi(args[1])
			if err != nil {
				fmt.Println("%s is not a number", args[1])
			}
			solutions.DayElevenPartTwo(args[0], num)
		} else {
			solutions.DayEleven(args[0])
		}
	},
}

func init() {
	dayEleven.Flags().BoolVar(&partTwo, "part2", false, "Run part 2 solution")
	rootCmd.AddCommand(dayEleven)
}
