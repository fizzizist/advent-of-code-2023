package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/fizzizist/advent-of-code-2023/pkg/solutions"
)

var partTwo bool

var dayOne = &cobra.Command{
	Use:   "day-1",
	Short: "Runs the day 1 puzzle.",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if partTwo {
			solutions.DayOnePartTwo(args[0])
		} else {
			solutions.DayOne(args[0])
		}
	},
}

func init() {
	dayOne.Flags().BoolVar(&partTwo, "part2", false, "Run part 2 solution")
	rootCmd.AddCommand(dayOne)
}
