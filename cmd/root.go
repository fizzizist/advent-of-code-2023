package cmd

import "github.com/spf13/cobra"

var rootCmd = &cobra.Command{
	Use:   "advent-of-code-2023",
	Short: "An app for running Advent of Code 2023 solutions",
}

func Execute() error {
	return rootCmd.Execute()
}
