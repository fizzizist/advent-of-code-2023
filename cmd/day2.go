package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/fizzizist/advent-of-code-2023/pkg/solutions"
)

var dayTwo = &cobra.Command{
	Use:   "day-2",
	Short: "Runs the day 1 puzzle.",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if partTwo {
			solutions.DayTwoPartTwo(args[0])
		} else {
			solutions.DayTwo(args[0])
		}
	},
}

func init() {
	dayTwo.Flags().BoolVar(&partTwo, "part2", false, "Run part 2 solution")
	rootCmd.AddCommand(dayTwo)
}
